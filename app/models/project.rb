class Project < ApplicationRecord
  after_update_commit {broadcast_update_to "projects"}
  after_destroy_commit {broadcast_remove_to "projects", target: "project_#{self.id}"}
  after_create_commit {broadcast_append_to "projects", target: "listing"}
end
